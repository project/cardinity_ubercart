CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Ubercart Cardinity is Drupal Ubercart payment module
that integrates Cardinity payment gateway into your Ubercart shop.
This is everything you need for accepting
card payments in your Drupal-based store without bureaucratic procedures.

Features:

  * Fully integrated into Ubercart Order processing workflow
  * Direct payment.
  * 3-D Secure support.


REQUIREMENTS
------------

This module requires the following modules:

 * Ubercart (https://drupal.org/project/ubercart)

   These Ubercart modules should be enabled:
   - uc_payment
   - uc_credit


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Enable Cardinity as your credit card payments provider at Store »
   Configuration » Payment methods » Credit card.

 * Configure Cardinity settings(the Cardinity link) with your API keys.

 * Choose Payment processing mode:

   - Live if you want to make real charges on your clients credit cards.

   - Test if you just want to test integration without real payments happening.


MAINTAINERS
-----------

Current maintainers:
 * Cardinity (cardinity) - https://www.drupal.org/user/3176383/
